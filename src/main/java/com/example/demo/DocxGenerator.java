package com.example.demo;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

@Component
public class DocxGenerator {

    @Autowired
    private ResourceLoader resourceLoader;

    private static final String FIRST_OPTION_TEMPLATE = "classpath:/firstOption.docx";
    private static final String SECOND_OPTION_TEMPLATE = "classpath:/secondOption.docx";
    private static final String TEMPLATE_NAME = "classpath:/template.docx";

    public byte[] generateDocxFileFromTemplate(UserInformation userInformation, String option) throws Exception {

        InputStream templateInputStream = resourceLoader.getResource(TEMPLATE_NAME).getInputStream();

        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(templateInputStream);

        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
        documentPart.getContent().add(3, getSides(option).get(0));

        VariablePrepare.prepare(wordMLPackage);

        HashMap<String, String> variables = new HashMap<>();
        variables.put("date", userInformation.getDate());
        variables.put("customerOrganizationName", userInformation.getCustomerOrganizationName());
        variables.put("executorOrganizationName", userInformation.getExecutorOrganizationName());

        documentPart.variableReplace(variables);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        wordMLPackage.save(outputStream);

        return outputStream.toByteArray();
    }

    @SneakyThrows
    private List<Object> getSides(String option) {
        InputStream templateInputStream;
        if (StringUtils.isEmpty(option) || StringUtils.equals(option, "firstOption")) {
            templateInputStream = resourceLoader.getResource(FIRST_OPTION_TEMPLATE).getInputStream();
        } else {
            templateInputStream = resourceLoader.getResource(SECOND_OPTION_TEMPLATE).getInputStream();
        }

        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(templateInputStream);

        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
        return documentPart.getContent();
    }

    private P createParagraph() {
        ObjectFactory factory = Context.getWmlObjectFactory();
        P paragraph = factory.createP();
        paragraph.setParaId("pp");
        Text text = factory.createText();
        text.setValue("Новый параграф");

        R run = factory.createR();
        run.getContent().add(text);
        paragraph.getContent().add(run);

        return paragraph;
    }
}
