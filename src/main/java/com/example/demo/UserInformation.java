package com.example.demo;

import lombok.Data;

@Data
public class UserInformation {

    private String customerOrganizationName;
    private String executorOrganizationName;
    private String date;

}
