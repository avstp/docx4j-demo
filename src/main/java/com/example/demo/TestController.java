package com.example.demo;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private DocxGenerator generator;

    @GetMapping("/hello")
    public String hello() {
        return "Hello";
    }

    @SneakyThrows
    @GetMapping("/doc")
    public ResponseEntity<?> createDoc(@RequestParam(required = false) String option) {
        UserInformation userInformation = new UserInformation();
        userInformation.setDate("18");
        userInformation.setCustomerOrganizationName("ООО ООО");
        userInformation.setExecutorOrganizationName("ООО GGG");
        byte[] bytes = generator.generateDocxFileFromTemplate(userInformation, option);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=example.docs");
        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new ByteArrayResource(bytes));
    }
}
